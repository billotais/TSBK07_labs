#version 150

out vec4 out_Color;
in  vec3 trans_Color;

void main(void)
{
	out_Color = vec4(trans_Color,1.0);
}
