// Lab 1-1.
// This is the same as the first simple example in the course book,
// but with a few error checks.
// Remember to copy your file to a new on appropriate places during the lab so you keep old results.
// Note that the files "lab1-1.frag", "lab1-1.vert" are required.

// Should work as is on Linux and Mac. MS Windows needs GLEW or glee.
// See separate Visual Studio version of my demos.
#ifdef __APPLE__
	#include <OpenGL/gl3.h>
	// Linking hint for Lightweight IDE
	// uses framework Cocoa
#endif
#include "MicroGlut.h"
#include "VectorUtils3.h"
#include "GL_utilities.h"
#include <math.h>
#include <stdio.h>

// Globals
// Data would normally be read from files
GLfloat vertices[] =
{
	0.5f,0.5f,-0.5,
	-0.5,0.5,-0.5,
	-0.5,0.5,0.5,
	
	0.5f,0.5f,-0.5,
	0.5,0.5,0.5,
	-0.5,0.5,0.5,

	0.5f,-0.5f,-0.5,
	-0.5,-0.5,-0.5,
	-0.5,-0.5,0.5,
	
	0.5f,-0.5f,-0.5,
	0.5,-0.5,0.5,
	-0.5,-0.5,0.5,

	0.5,0.5,-0.5,
	0.5,0.5,0.5,
	0.5,-0.5,0.5,
	
	0.5,0.5,-0.5,
	0.5,-0.5,-0.5,
	0.5,-0.5,0.5,

	-0.5,0.5,-0.5,
	-0.5,0.5,0.5,
	-0.5,-0.5,0.5,
	
	-0.5,0.5,-0.5,
	-0.5,-0.5,-0.5,
	-0.5,-0.5,0.5,

	0.5,0.5,0.5,
	-0.5,0.5,0.5,
	-0.5,-0.5,0.5,

	0.5,0.5,0.5,
	0.5,-0.5,0.5,
	-0.5,-0.5,0.5,

	0.5,0.5,-0.5,
	-0.5,0.5,-0.5,
	-0.5,-0.5,-0.5,

	0.5,0.5,-0.5,
	0.5,-0.5,-0.5,
	-0.5,-0.5,-0.5
	

	

		
};

GLfloat colors[36*3];
double angle=0;
  
// Reference to shader program
	GLuint program;

// vertex array object
unsigned int vertexArrayObjID;

void init(void)
{
	int i;
	for (i = 0; i < 36*3; ++i)
	{
		colors[i] = (double)rand()/RAND_MAX;
	}
	// vertex buffer object, used for uploading the geometry
	unsigned int vertexBufferObjID;
	unsigned int vertexBufferColor;
	

	dumpInfo();

	// GL inits
	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
	glEnable(GL_DEPTH_TEST);
	glClearColor(0.2,0.2,0.5,0);
	//glDisable(GL_DEPTH_TEST);
	printError("GL inits");

	// Load and compile shader
	program = loadShaders("lab1-5.vert", "lab1-5.frag");
	printError("init shader");
	
	// Upload geometry to the GPU:
	
	// Allocate and activate Vertex Array Object
	glGenVertexArrays(1, &vertexArrayObjID);
	glBindVertexArray(vertexArrayObjID);
	// Allocate Vertex Buffer Objects
	glGenBuffers(1, &vertexBufferObjID);
	
	
	// VBO for vertex data
	glBindBuffer(GL_ARRAY_BUFFER, vertexBufferObjID);
	
	glBufferData(GL_ARRAY_BUFFER, 3*36*sizeof(GLfloat), vertices, GL_STATIC_DRAW);
	

	glVertexAttribPointer(glGetAttribLocation(program, "in_Position"), 3, GL_FLOAT, GL_FALSE, 0, 0); 
	
	glEnableVertexAttribArray(glGetAttribLocation(program, "in_Position"));
	

	glGenBuffers(1, &vertexBufferColor);
	glBindBuffer(GL_ARRAY_BUFFER, vertexBufferColor);
	glBufferData(GL_ARRAY_BUFFER, 3*36*sizeof(GLfloat), colors, GL_STATIC_DRAW);
glVertexAttribPointer(glGetAttribLocation(program, "in_Color"), 3, GL_FLOAT, GL_FALSE, 0, 0);
glEnableVertexAttribArray(glGetAttribLocation(program, "in_Color"));
	// End of upload of geometry
	
	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
	glEnable(GL_DEPTH_TEST);
	printError("init arrays");
}
void OnTimer(int value)

{

    glutPostRedisplay();

    glutTimerFunc(20, &OnTimer, value);

}

void display(void)
{
	
glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
	glEnable(GL_DEPTH_TEST);
GLfloat myMatrix2[] = {    cos(angle), -sin(angle), 0.0f, 0.0f,

                        sin(angle), cos(angle), 0.0f, 0.0f,

                        0.0f, 0.0f,1.0f, 0.0f,

                        0.0f, 0.0f, 0.0f, 1.0f };
GLfloat myMatrix[] = {    1.0f, 0.0, 0.0, 0.0,
			0.0f, cos(angle), -sin(angle), 0.0f,

                        0.0f, sin(angle), cos(angle), 0.0f,

                  

                        0.0f, 0.0f, 0.0f, 1.0f };
	printError("pre display");

	// clear the screen
	
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	glUniformMatrix4fv(glGetUniformLocation(program, "myMatrix"), 1, GL_TRUE, myMatrix);
	glUniformMatrix4fv(glGetUniformLocation(program, "myMatrix2"), 1, GL_TRUE, myMatrix2);

	glBindVertexArray(vertexArrayObjID);	// Select VAO
	glDrawArrays(GL_TRIANGLES, 0, 36);	// draw object
	
	printError("display");
	angle= angle +0.01;
	glutSwapBuffers();
}

int main(int argc, char *argv[])
{
	glutInit(&argc, argv);
	glutInitContextVersion(3, 2);
	glutCreateWindow ("GL3 white triangle example");
	glutDisplayFunc(display); 
	init ();
	glutTimerFunc(20, &OnTimer, 0);
	glutMainLoop();
	return 0;
}
