#version 150

out vec4 out_Color;
in  vec3 trans_Normal;

void main(void)
{
	out_Color = vec4(trans_Normal,1.0);
}
