#version 150

in  vec3 in_Position; 
in  vec3 in_Color;
out vec3 trans_Color;
uniform mat4 myMatrix;
uniform mat4 myMatrix2;

void main(void)
{
	gl_Position = (myMatrix*myMatrix2* vec4(in_Position, 1.0));
	trans_Color = in_Color;
	
}
