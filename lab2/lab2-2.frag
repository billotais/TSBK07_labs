#version 150
in  vec2 transfer;
out vec4 out_Color;
in  vec3 trans_Normal;
uniform sampler2D texUnit;	

void main(void)
{
	out_Color= texture(texUnit, transfer);
	
}
