#version 150
in  vec2 transfer;
out vec4 out_Color;
in  vec3 trans_Normal;
in vec3 transfer_light;
uniform sampler2D texUnit;	

void main(void)
{
	//out_Color= texture(texUnit, transfer);
	out_Color = vec4(transfer_light, 1.0);
}
