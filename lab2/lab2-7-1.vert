#version 150
in vec3 in_vertex1;
in vec3 in_normal1;
in vec3 in_texture1;
uniform mat4 cameraMatrix1;
uniform mat4 projMatrix1;
uniform mat4 transformMatrix1;
out vec3 transfer_light1;

 


void main(void)
{

	const vec3 light = vec3(0.58, 0.58, 0.58);

	vec3 transformedNormal1 = mat3(projMatrix1*cameraMatrix1*transformMatrix1)*in_normal1; 
	gl_Position = (projMatrix1*cameraMatrix1*transformMatrix1* vec4(in_vertex1, 1.0));


	float oneCoordLight1 = dot(normalize(transformedNormal1), normalize(light - vec3(projMatrix1*cameraMatrix1* vec4(in_vertex1, 1.0)))); 
	transfer_light1 = vec3(oneCoordLight1,oneCoordLight1,oneCoordLight1);

	
	
}
