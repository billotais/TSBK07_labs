#version 150
in vec3 in_vertex2;
in vec3 in_normal2;
in vec3 in_texture2;
uniform mat4 cameraMatrix2;
uniform mat4 projMatrix2;
uniform mat4 transformMatrix2;
out vec3 transfer_light2;
 


void main(void)
{

	const vec3 light2 = vec3(0.58, 0.58, 0.58);

	vec3 transformedNormal2 = mat3(projMatrix2*cameraMatrix2*transformMatrix2)*in_normal2; 
	gl_Position = (projMatrix2*cameraMatrix2*transformMatrix2* vec4(in_vertex2, 1.0));


	float oneCoordLight2 = dot(normalize(transformedNormal2), normalize(light2 - vec3(projMatrix2*cameraMatrix2* vec4(in_vertex2, 1.0)))); 
	transfer_light2 = vec3(oneCoordLight2,oneCoordLight2,oneCoordLight2);
	
	
}
