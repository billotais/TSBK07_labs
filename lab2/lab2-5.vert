#version 150
in  vec2 inTexCoord;
in  vec3 in_Position; 
in  vec3 in_Normal;
out vec3 trans_Normal;
out vec2 transfer;
out vec3 transfer_light;
uniform mat4 projMatrix;
uniform mat4 cameraMatrix;

 


void main(void)
{

	const vec3 light = vec3(0.58, 0.58, 0.58);
	vec3 transformedNormal = mat3(projMatrix*cameraMatrix)*in_Normal; 
	gl_Position = (projMatrix*cameraMatrix* vec4(in_Position, 1.0));
	
	//trans_Normal = in_Normal;
	//transfer= inTexCoord;

	float oneCoordLight = dot(normalize(transformedNormal), normalize(light - vec3(projMatrix*cameraMatrix* vec4(in_Position, 1.0)))); 
	transfer_light = vec3(oneCoordLight,oneCoordLight,oneCoordLight);
	
	
}
