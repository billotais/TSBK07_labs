#version 150
in  vec2 inTexCoord;
in  vec3 in_Position; 
in  vec3 in_Normal;
out vec3 trans_Normal;
out vec2 transfer;
uniform mat4 mdlMatrix;
uniform mat4 projMatrix;
 


void main(void)
{
	gl_Position = (projMatrix*mdlMatrix* vec4(in_Position, 1.0));
	
	trans_Normal = in_Normal;
	transfer= inTexCoord;
	
	
}
