// Lab 1-1.
// This is the same as the first simple example in the course book,
// but with a few error checks.
// Remember to copy your file to a new on appropriate places during the lab so you keep old results.
// Note that the files "lab1-1.frag", "lab1-1.vert" are required.

// Should work as is on Linux and Mac. MS Windows needs GLEW or glee.
// See separate Visual Studio version of my demos.
#ifdef __APPLE__
	#include <OpenGL/gl3.h>
	// Linking hint for Lightweight IDE
	// uses framework Cocoa
#endif
#include "MicroGlut.h"
#include "VectorUtils3.h"
#include "GL_utilities.h"
#include <math.h>
#include <stdio.h>
#include "loadobj.h"
#include "LoadTGA.h"


// Globals
// Data would normally be read from files

#define near 1.0

#define far 30.0

#define right 0.5

#define left -0.5

#define top 0.5

#define bottom -0.5

Model *m1;
Model *m2;


double angle=0;
  
// Reference to shader program
	GLuint program1;
	GLuint program2;
	

// vertex array object

	mat4 camera;
	mat4 rotate_camera;

	vec3 pos;
	vec3 lookat;
	vec3 rotnormal;

mat4 model1_transform;
mat4 model2_transform;



void init(void)
{
	GLfloat projectionMatrix[] = {          2.0f*near/(right-left), 0.0f, (right+left)/(right-left), 0.0f,

                                            0.0f, 2.0f*near/(top-bottom), (top+bottom)/(top-bottom), 0.0f,

                                            0.0f, 0.0f, -(far + near)/(far - near), -2*far*near/(far - near),

                                            0.0f, 0.0f, -1.0f, 0.0f };

	rotate_camera = Ry(angle);

	//pos = {5.0, 5.0, 5.0};
	pos.x = 8.0;pos.y = 8.0;pos.z=8.0;
	lookat.x = 0.0; lookat.y = 0.0; lookat.z = 0.0;
	rotnormal.x = 0.0; rotnormal.y = 1.0; lookat.z = 0.0;
	camera = lookAtv(MultVec3(rotate_camera, pos), lookat, rotnormal);
	
	m1 = LoadModelPlus("bunny.obj");
	m2 = LoadModelPlus("bilskiss.obj");
	// vertex buffer object, used for uploading the geometry



	dumpInfo();

	// GL inits
	glClearColor(0.2,0.2,0.5,0);
	glDisable(GL_DEPTH_TEST);
	printError("GL inits");

	// Load and compile shader
	program1 = loadShaders("lab2-7-1.vert", "lab2-7-1.frag");
	//program2 = loadShaders("lab2-7-2.vert", "lab2-7-2.frag");
	printError("init shader");


	
	// Upload geometry to the GPU:
	
		

	mat4 model1_pos = T(0, 0, 2);
	mat4 model2_pos = T(0, 3, 0);
	
	mat4 model1_rot = Rx(0.7);
	mat4 model2_rot = Ry(0.7);

	model1_transform = Mult(model1_pos, model1_rot); 
	model2_transform = Mult(model2_pos, model2_rot); 
	
	// Upload the transformation matrices
	DrawModel(m1, program1, "in_vertex1",  "in_normal1", "in_texture1");
    glUniformMatrix4fv(glGetUniformLocation(program1, "cameraMatrix1"), 1, GL_TRUE, camera.m);
    glUniformMatrix4fv(glGetUniformLocation(program1, "projMatrix1"), 1, GL_TRUE, projectionMatrix);
	
	glUniformMatrix4fv(glGetUniformLocation(program1, "transformMatrix1"), 1, GL_TRUE, model1_transform.m);	
	glDrawElements(GL_TRIANGLES, m1->numIndices, GL_UNSIGNED_INT, 0L);

	
	DrawModel(m2, program1, "in_vertex1",  "in_normal1", "in_texture1");	
	glUniformMatrix4fv(glGetUniformLocation(program1, "transformMatrix1"), 1, GL_TRUE, model2_transform.m);	
	glDrawElements(GL_TRIANGLES, m2->numIndices, GL_UNSIGNED_INT, 0L);

	// End of upload of geometry
	
    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
	glEnable(GL_DEPTH_TEST);
	glActiveTexture(GL_TEXTURE0);
	printError("init arrays");
}
void OnTimer(int value)

{

    glutPostRedisplay();

    glutTimerFunc(20, &OnTimer, value);

}

void display(void)
{
	

	printError("pre display");

	// clear the screen
	
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	rotate_camera = Ry(angle);

	camera = lookAtv(MultVec3(rotate_camera, pos), lookat, rotnormal);

	
	glUniformMatrix4fv(glGetUniformLocation(program1, "transformMatrix1"), 1, GL_TRUE, model1_transform.m);
	glUniformMatrix4fv(glGetUniformLocation(program1, "cameraMatrix1"), 1, GL_TRUE, camera.m);
  	
	DrawModel(m1, program1, "in_vertex1",  "in_normal1", "in_texture1");
	

	
	glUniformMatrix4fv(glGetUniformLocation(program1, "transformMatrix1"), 1, GL_TRUE, model2_transform.m);
	glUniformMatrix4fv(glGetUniformLocation(program1, "cameraMatrix1"), 1, GL_TRUE, camera.m);
	
	DrawModel(m2, program1, "in_vertex1",  "in_normal1", "in_texture1");

	
	printError("display");
	angle= angle +0.03;
	glutSwapBuffers();
}

int main(int argc, char *argv[])
{
	glutInit(&argc, argv);
	glutInitContextVersion(3, 2);
	glutCreateWindow ("GL3 white triangle example");
	glutDisplayFunc(display); 
	init ();
	glutTimerFunc(20, &OnTimer, 0);
	glutMainLoop();
	return 0;
}
