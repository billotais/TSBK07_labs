#version 150

in vec3 transfer_normal_nt;
in vec4 transfer_vertex_transformed_nt;
in vec2 transfer_texture;
out vec4 out_Color_nt;


uniform vec3 lightSourcesDirPosArr[4];
uniform vec3 lightSourcesColorArr[4];
uniform float specularExponent;
uniform bool isDirectional[4];

uniform mat4 cameraMatrix_nt;
uniform mat4 projMatrix_nt;
uniform mat4 transformMatrix_nt;

uniform sampler2D texUnit_nt_0;
uniform sampler2D texUnit_nt_1;

void main(void)
{
	vec3 temp_color = vec3(0, 0, 0);
	for (int i = 0; i < 4; ++i)
	{

		vec3 light_transformed = vec3(projMatrix_nt*cameraMatrix_nt*vec4(lightSourcesDirPosArr[i], 1.0));
		vec3 vertex_to_light;
		if (isDirectional[i])
		{
			vertex_to_light = normalize(lightSourcesDirPosArr[i]);
		}
		else
		{
			vertex_to_light = normalize(light_transformed - vec3(transfer_vertex_transformed_nt));
		}

		vec3 light_level = lightSourcesColorArr[i];
		vec3 reflectivity = vec3(0.3, 0.3, 0.3);
		float costheta = dot(normalize(transfer_normal_nt),vertex_to_light);
		vec3 diffuse = vec3(max(0, reflectivity.x*light_level.x*costheta), max(0, reflectivity.y*light_level.y*costheta),max(0,reflectivity.z*light_level.z*costheta));

		vec3 r = normalize(2*normalize(transfer_normal_nt*(dot(vertex_to_light,normalize(transfer_normal_nt)))) - vertex_to_light);
		vec3 vertex_to_camera = normalize(vec3(projMatrix_nt*cameraMatrix_nt*cameraMatrix_nt[0]) - vec3(transfer_vertex_transformed_nt));
		vec3 specular = vec3(reflectivity.x*light_level.x*pow(max(0,dot(r, vertex_to_camera)), specularExponent),
							reflectivity.y*light_level.y*pow(max(0,dot(r, vertex_to_camera)), specularExponent),
							reflectivity.z*light_level.z*pow(max(0,dot(r, vertex_to_camera)), specularExponent));

		/*vec3 specular = vec3(reflectivity.x*light_level.x*pow(abs(dot(r, vertex_to_light)), specularExponent),
							reflectivity.y*light_level.y*pow(abs(dot(r, vertex_to_light)), specularExponent),
							reflectivity.z*light_level.z*pow(abs(dot(r, vertex_to_light)), specularExponent));*/


		temp_color += diffuse + specular;

	}






	out_Color_nt = 0.5*vec4(temp_color,1.0) + 0.3*texture(texUnit_nt_0, transfer_texture) + 0.2*texture(texUnit_nt_1, transfer_texture);

}
