#version 150

in vec3 transfer_light1;
out vec4 out_Color1;
void main(void)
{
	
	out_Color1 = vec4(transfer_light1, 1.0);
}
