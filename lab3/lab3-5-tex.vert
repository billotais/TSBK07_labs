#version 150
in vec3 in_vertex_t;
in vec3 in_normal_t;
in vec2 in_texture_t;
uniform mat4 cameraMatrix_t;
uniform mat4 projMatrix_t;
uniform mat4 transformMatrix_t;
out vec3 transfer_light_t;
out vec2 transfer_tex_coord_t;




void main(void)
{

	const vec3 light_t = vec3(0.58, 0.58, 0.58);

	vec3 transformedNormal_t = mat3(projMatrix_t*cameraMatrix_t*transformMatrix_t)*in_normal_t;
	gl_Position = (projMatrix_t*cameraMatrix_t*transformMatrix_t* vec4(in_vertex_t, 1.0));


	float oneCoordLight_t = dot(normalize(transformedNormal_t), normalize(light_t - vec3(projMatrix_t*cameraMatrix_t* vec4(in_vertex_t, 1.0))));
	transfer_light_t = vec3(oneCoordLight_t,oneCoordLight_t,oneCoordLight_t);
    transfer_tex_coord_t = in_texture_t;

}
