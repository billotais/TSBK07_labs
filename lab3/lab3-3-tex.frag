#version 150

in vec3 transfer_light_t;
in vec2 transfer_tex_coord_t;
out vec4 out_Color_t;
uniform sampler2D texUnit_t;
void main(void)
{

	out_Color_t = texture(texUnit_t, transfer_tex_coord_t);
	//out_Color_t = vec4(1.0, 1.0, 1.0, 1.0);
}
