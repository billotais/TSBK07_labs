// Lab 1-1.
// This is the same as the first simple example in the course book,
// but with a few error checks.
// Remember to copy your file to a new on appropriate places during the lab so you keep old results.
// Note that the files "lab1-1.frag", "lab1-1.vert" are required.

// Should work as is on Linux and Mac. MS Windows needs GLEW or glee.
// See separate Visual Studio version of my demos.
#ifdef __APPLE__
	#include <OpenGL/gl3.h>
	// Linking hint for Lightweight IDE
	// uses framework Cocoa
#endif
#include "MicroGlut.h"
#include "VectorUtils3.h"
#include "GL_utilities.h"
#include <math.h>
#include <stdio.h>
#include "loadobj.h"
#include "LoadTGA.h"


// Globals
// Data would normally be read from files

#define near 1.0

#define far 500.0

#define right 0.5

#define left -0.5

#define top 0.5

#define bottom -0.5

#define PI 3.1415

Model *blade;
Model *roof;
Model *walls;
Model *balcony;
Model *ground;
Model *skybox;
Model *bunny;


double angle=0;

// Reference to shader program
GLuint program_notex;
GLuint program_tex;



// Camera variables
mat4 camera;
vec3 pos;
vec3 lookat;
vec3 rotnormal;

// Objects variables
mat4 walls_transform;
mat4 roof_transform;
mat4 balcony_transform;
mat4 blade_transform;
mat4 ground_transform;
mat4 sky_transform;
mat4 bunny_transform;

// Blade specific variables
mat4 blade_turning;
mat4 blade_pos;
mat4 blade_rot;

// Camera control varaibles
mat4 camera_rotation_side;

double camera_x = -50;
double camera_z = 0;
double camera_side_angle = 0.0;


// Skz box variable
mat4 sky_scale;




// Textures
GLuint 	grassTex;
GLuint  skyTex;

// Ground Definitation
#define G 500.0
GLfloat ground_vertex[] =
{
	-G,0.0f,G,
	G,0.0f,-G,
	-G,0.0f,-G,

	-G,0.0f,G,
	G,0.0f,G,
	G,0.0f,-G
};
GLfloat ground_tex[] =
{
	-1.0f,1.0f,
	-1.0f,-1.0f,
	1.0f, -1.0f,
	-1.0f,1.0f,
	1.0f, -1.0f,
	1.0f, 1.0f
};
GLfloat ground_normal[] =
{
	0.0, 1.0, 0.0,
	0.0, 1.0, 0.0,
	0.0, 1.0, 0.0,
	0.0, 1.0, 0.0,
	0.0, 1.0, 0.0,
	0.0, 1.0, 0.0
};
GLuint ground_indices[] = {0, 1, 2, 3, 4, 5};

void init(void)
{
	GLfloat projectionMatrix[] = {
		2.0f*near/(right-left), 0.0f, (right+left)/(right-left), 0.0f,
		0.0f, 2.0f*near/(top-bottom), (top+bottom)/(top-bottom), 0.0f,
		0.0f, 0.0f, -(far + near)/(far - near), -2*far*near/(far - near),
		0.0f, 0.0f, -1.0f, 0.0f };



	// Camera position
	pos.x = camera_x;pos.y = 4.0;pos.z=camera_z;
	lookat.x = 0.0; lookat.y = 0.0; lookat.z = 0.0;
	rotnormal.x = 0.0; rotnormal.y = 1.0; lookat.z = 0.0;
	camera = lookAtv(pos, lookat, rotnormal);

	// Load al the models
	blade = LoadModelPlus("windmill/blade.obj");
	roof = LoadModelPlus("windmill/windmill-roof.obj");
	walls = LoadModelPlus("windmill/windmill-walls.obj");
	balcony = LoadModelPlus("windmill/windmill-balcony.obj");
	skybox = LoadModelPlus("skybox.obj");
	bunny = LoadModelPlus("bunny.obj");
	ground = LoadDataToModel(
			ground_vertex,
			ground_normal,
			ground_tex,
			ground_normal,
			ground_indices,
			6,
			6);

	// Load the textures
	LoadTGATextureSimple("grass.tga", &grassTex);

	LoadTGATextureSimple("SkyBox512.tga", &skyTex);


	dumpInfo();

	// GL inits
	glClearColor(0.2,0.2,0.5,0);
	glDisable(GL_DEPTH_TEST);
	printError("GL inits");

	// Load and compile shader
	program_notex = loadShaders("lab3-3-notex.vert", "lab3-3-notex.frag");
	program_tex = loadShaders("lab3-3-tex.vert", "lab3-3-tex.frag");

	printError("init shader");



	// Upload geometry to the GPU:

	// Set the default positions of differents objects

	mat4 walls_pos = T(0, 0, 0);
	mat4 roof_pos = T(0, 0, 0);
	mat4 balcony_pos = T(0, 0, 0.0);
	blade_pos = T(-4.8, 9.0, 0.0);
	mat4 ground_pos = T(0.0, 0.0, 0.0);
	mat4 bunny_pos = T(0, 0, 10);


	mat4 walls_rot = Ry(PI/10);
	mat4 roof_rot = Ry(0.0);
	mat4 balcony_rot = Ry(PI);
	blade_rot = Rx(PI/2);
	mat4 ground_rot = Ry(1.0);
	mat4 bunny_rot = Ry(1.2);

	sky_scale = S(70, 70, 70);

	walls_transform = Mult(walls_pos, walls_rot);
	roof_transform = Mult(roof_pos, roof_rot);
	balcony_transform = Mult(balcony_pos, balcony_rot);
	blade_transform = Mult(blade_pos, blade_rot);
	ground_transform = Mult(ground_pos, ground_rot);
	bunny_transform = Mult(bunny_pos, bunny_rot);
	sky_transform = Mult(T(camera.m[0], camera.m[1], camera.m[2]), sky_scale);

	// Upload the transformation matrices for the camera
	glUseProgram(program_notex);
	glUniformMatrix4fv(glGetUniformLocation(program_notex, "cameraMatrix_nt"), 1, GL_TRUE, camera.m);
    glUniformMatrix4fv(glGetUniformLocation(program_notex, "projMatrix_nt"), 1, GL_TRUE, projectionMatrix);
	glUseProgram(program_tex);
	glUniformMatrix4fv(glGetUniformLocation(program_tex, "cameraMatrix_t"), 1, GL_TRUE, camera.m);
    glUniformMatrix4fv(glGetUniformLocation(program_tex, "projMatrix_t"), 1, GL_TRUE, projectionMatrix);

	// Bind the textures
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, skyTex);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, grassTex);


    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
	glEnable(GL_DEPTH_TEST);
	glActiveTexture(GL_TEXTURE0);
	printError("init arrays");
}
void OnTimer(int value)

{

    glutPostRedisplay();

    glutTimerFunc(20, &OnTimer, value);

}

void display(void)
{


	printError("pre display");

	// clear the screen

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);



	if (glutKeyIsDown('w'))
	{
		camera_x += 0.3;
	}
	if (glutKeyIsDown('s'))
	{
		camera_x -= 0.3;
	}
	if (glutKeyIsDown('a'))
	{
		camera_z-= 0.3;
	}
	if (glutKeyIsDown('d'))
	{
		camera_z += 0.3;
	}

	//camera_rotation_side = Ry(camera_side_angle);
	//camera_rotation_vertical = Rz(camera_vertical_angle);
	//camera = lookAtv(MultVec3(Mult(camera_rotation_vertical, camera_rotation_side), pos), lookat, rotnormal);
	mat4 camera_pos_moved = T(camera_x, 0, camera_z);
	camera = lookAtv(MultVec3(camera_pos_moved, pos), MultVec3(camera_pos_moved, lookat), rotnormal);
	glUseProgram(program_notex);
	glUniformMatrix4fv(glGetUniformLocation(program_notex, "cameraMatrix_nt"), 1, GL_TRUE, camera.m);
	glUseProgram(program_tex);
	glUniformMatrix4fv(glGetUniformLocation(program_tex, "cameraMatrix_t"), 1, GL_TRUE, camera.m);



	sky_transform = Mult(camera_pos_moved, sky_scale);


	// Draw skybox
	glUseProgram(program_tex);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);
	glUniform1i(glGetUniformLocation(program_tex, "texUnit_t"), 0);
	glUniformMatrix4fv(glGetUniformLocation(program_tex, "transformMatrix_t"), 1, GL_TRUE,  sky_transform.m);
	DrawModel(skybox,program_tex, "in_vertex_t",  "in_normal_t", "in_texture_t");
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);

	glUseProgram(program_notex);
	// Draw windmill
	glUniformMatrix4fv(glGetUniformLocation(program_notex, "transformMatrix_nt"), 1, GL_TRUE, walls_transform.m);
	DrawModel(walls, program_notex, "in_vertex_nt",  "in_normal_nt", "in_texture_nt");

	glUniformMatrix4fv(glGetUniformLocation(program_notex, "transformMatrix_nt"), 1, GL_TRUE, roof_transform.m);
	DrawModel(roof, program_notex, "in_vertex_nt",  "in_normal_nt", "in_texture_nt");

	glUniformMatrix4fv(glGetUniformLocation(program_notex, "transformMatrix_nt"), 1, GL_TRUE, balcony_transform.m);
	DrawModel(balcony, program_notex, "in_vertex_nt",  "in_normal_nt", "in_texture_nt");



	// Draw blades

	blade_rot = Rx(PI/2);
	blade_turning = Rx(angle);

	blade_turning = Mult(blade_turning, blade_rot);
	blade_transform = Mult(blade_pos, blade_turning);
	glUniformMatrix4fv(glGetUniformLocation(program_notex, "transformMatrix_nt"), 1, GL_TRUE, blade_transform.m);
	DrawModel(blade, program_notex, "in_vertex_nt",  "in_normal_nt", "in_texture_nt");


	blade_turning = Mult(blade_turning, blade_rot);
	blade_transform = Mult(blade_pos, blade_turning);
	glUniformMatrix4fv(glGetUniformLocation(program_notex, "transformMatrix_nt"), 1, GL_TRUE, blade_transform.m);
	DrawModel(blade, program_notex, "in_vertex_nt",  "in_normal_nt", "in_texture_nt");

	blade_turning = Mult(blade_turning, blade_rot);
	blade_transform = Mult(blade_pos, blade_turning);
	glUniformMatrix4fv(glGetUniformLocation(program_notex, "transformMatrix_nt"), 1, GL_TRUE, blade_transform.m);
	DrawModel(blade, program_notex, "in_vertex_nt",  "in_normal_nt", "in_texture_nt");

	blade_turning = Mult(blade_turning, blade_rot);
	blade_transform = Mult(blade_pos, blade_turning);
	glUniformMatrix4fv(glGetUniformLocation(program_notex, "transformMatrix_nt"), 1, GL_TRUE, blade_transform.m);
	DrawModel(blade, program_notex, "in_vertex_nt",  "in_normal_nt", "in_texture_nt");

	glUniformMatrix4fv(glGetUniformLocation(program_notex, "transformMatrix_nt"), 1, GL_TRUE, bunny_transform.m);
	DrawModel(bunny, program_notex, "in_vertex_nt",  "in_normal_nt", "in_texture_nt");

	// Draw ground
	glUseProgram(program_tex);
	glUniform1i(glGetUniformLocation(program_tex, "texUnit_t"), 1);
	glUniformMatrix4fv(glGetUniformLocation(program_tex, "transformMatrix_t"), 1, GL_TRUE, ground_transform.m);
	DrawModel(ground, program_tex, "in_vertex_t",  "in_normal_t", "in_texture_t");


	printError("display");
	angle += 0.03;
	glutSwapBuffers();
}

int main(int argc, char *argv[])
{
	glutInit(&argc, argv);
	glutInitContextVersion(3, 2);
	glutCreateWindow ("GL3 white triangle example");
	glutDisplayFunc(display);
	init ();
	glutTimerFunc(20, &OnTimer, 0);
	glutMainLoop();
	return 0;
}
