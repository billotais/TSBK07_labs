#version 150
in vec3 in_vertex_nt;
in vec3 in_normal_nt;
in vec2 in_texture_nt;
uniform mat4 cameraMatrix_nt;
uniform mat4 projMatrix_nt;
uniform mat4 transformMatrix_nt;



out vec3 transfer_normal_nt;
out vec4 transfer_vertex_transformed_nt;
out vec2 transfer_texture;



void main(void)
{



	transfer_normal_nt = mat3(projMatrix_nt*cameraMatrix_nt*transformMatrix_nt)*in_normal_nt;
	vec4 vertex_transformed = (projMatrix_nt*cameraMatrix_nt*transformMatrix_nt* vec4(in_vertex_nt, 1.0));
	gl_Position = vertex_transformed;
	transfer_vertex_transformed_nt = vertex_transformed;
	transfer_texture = in_texture_nt;




}
