#version 150

in vec3 transfer_light_nt;
in vec2 transfer_tex_coord_nt;
out vec4 out_Color_nt;
uniform sampler2D texUnit_nt;
void main(void)
{
	out_Color_nt = vec4(transfer_light_nt, 1.0);
	
}
