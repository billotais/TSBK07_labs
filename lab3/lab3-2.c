// Lab 1-1.
// This is the same as the first simple example in the course book,
// but with a few error checks.
// Remember to copy your file to a new on appropriate places during the lab so you keep old results.
// Note that the files "lab1-1.frag", "lab1-1.vert" are required.

// Should work as is on Linux and Mac. MS Windows needs GLEW or glee.
// See separate Visual Studio version of my demos.
#ifdef __APPLE__
	#include <OpenGL/gl3.h>
	// Linking hint for Lightweight IDE
	// uses framework Cocoa
#endif
#include "MicroGlut.h"
#include "VectorUtils3.h"
#include "GL_utilities.h"
#include <math.h>
#include <stdio.h>
#include "loadobj.h"
#include "LoadTGA.h"


// Globals
// Data would normally be read from files

#define near 1.0

#define far 40.0

#define right 0.5

#define left -0.5

#define top 0.5

#define bottom -0.5

#define PI 3.1415

Model *blade;
Model *roof;
Model *walls;
Model *balcony;


double angle=0;
  
// Reference to shader program
	GLuint program1;
	GLuint program2;
	

// vertex array object

	mat4 camera;


	vec3 pos;
	vec3 lookat;
	vec3 rotnormal;

mat4 walls_transform;
mat4 roof_transform;
mat4 balcony_transform;
mat4 blade_transform;

mat4 blade_turning;
mat4 blade_pos;
mat4 blade_rot;

mat4 camera_rotation_side;
mat4 camera_rotation_vertical;

double camera_side_angle = 0.0;
double camera_vertical_angle = 0.0;



void init(void)
{
	GLfloat projectionMatrix[] = {      2.0f*near/(right-left), 0.0f, (right+left)/(right-left), 0.0f,

                                            0.0f, 2.0f*near/(top-bottom), (top+bottom)/(top-bottom), 0.0f,

                                            0.0f, 0.0f, -(far + near)/(far - near), -2*far*near/(far - near),

                                            0.0f, 0.0f, -1.0f, 0.0f };

	

	
	pos.x = -30.0;pos.y = 0.0;pos.z=15.0;
	lookat.x = 0.0; lookat.y = 0.0; lookat.z = 0.0;
	rotnormal.x = 0.0; rotnormal.y = 1.0; lookat.z = 0.0;
	camera = lookAtv(pos, lookat, rotnormal);
	
	blade = LoadModelPlus("windmill/blade.obj");
	roof = LoadModelPlus("windmill/windmill-roof.obj");
	walls = LoadModelPlus("windmill/windmill-walls.obj");
	balcony = LoadModelPlus("windmill/windmill-balcony.obj");
	// vertex buffer object, used for uploading the geometry



	dumpInfo();

	// GL inits
	glClearColor(0.2,0.2,0.5,0);
	glDisable(GL_DEPTH_TEST);
	printError("GL inits");

	// Load and compile shader
	program1 = loadShaders("lab3-1.vert", "lab3-1.frag");
	
	printError("init shader");


	
	// Upload geometry to the GPU:
	
		

	mat4 walls_pos = T(0, -7, 0);
	mat4 roof_pos = T(0, -7, 0);
	mat4 balcony_pos = T(0, -7.0, 0.0);
	blade_pos = T(-4.8, 2.0, 0.0);
	
	mat4 walls_rot = Ry(PI/10);
	mat4 roof_rot = Ry(0.0);
	mat4 balcony_rot = Ry(PI);
	blade_rot = Rx(PI/2);

	walls_transform = Mult(walls_pos, walls_rot); 
	roof_transform = Mult(roof_pos, roof_rot);
	balcony_transform = Mult(balcony_pos, balcony_rot);  
	blade_transform = Mult(blade_pos, blade_rot);  
	
	// Upload the transformation matrices
	glUniformMatrix4fv(glGetUniformLocation(program1, "cameraMatrix1"), 1, GL_TRUE, camera.m);
    glUniformMatrix4fv(glGetUniformLocation(program1, "projMatrix1"), 1, GL_TRUE, projectionMatrix);

	
	glUniformMatrix4fv(glGetUniformLocation(program1, "transformMatrix1"), 1, GL_TRUE, walls_transform.m);	
	DrawModel(walls, program1, "in_vertex1",  "in_normal1", "in_texture1");

	
	
	glUniformMatrix4fv(glGetUniformLocation(program1, "transformMatrix1"), 1, GL_TRUE, roof_transform.m);	
	DrawModel(roof, program1, "in_vertex1",  "in_normal1", "in_texture1");

	
	glUniformMatrix4fv(glGetUniformLocation(program1, "transformMatrix1"), 1, GL_TRUE, balcony_transform.m);	
	DrawModel(balcony, program1, "in_vertex1",  "in_normal1", "in_texture1");

	glUniformMatrix4fv(glGetUniformLocation(program1, "transformMatrix1"), 1, GL_TRUE, blade_transform.m);	
	DrawModel(blade, program1, "in_vertex1",  "in_normal1", "in_texture1");

	// End of upload of geometry
	
    	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
	glEnable(GL_DEPTH_TEST);
	glActiveTexture(GL_TEXTURE0);
	printError("init arrays");
}
void OnTimer(int value)

{

    glutPostRedisplay();

    glutTimerFunc(20, &OnTimer, value);

}

void display(void)
{
	

	printError("pre display");

	// clear the screen
	
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	


	if (glutKeyIsDown('w'))
	{
		camera_vertical_angle += 0.02;
		
	}
	if (glutKeyIsDown('s'))
	{
		camera_vertical_angle -= 0.02;
	}
	if (glutKeyIsDown('a'))
	{
		camera_side_angle -= 0.02;
	}
	if (glutKeyIsDown('d'))
	{
		camera_side_angle += 0.02;
	}

	camera_rotation_side = Ry(camera_side_angle);
	camera_rotation_vertical = Rz(camera_vertical_angle);
	camera = lookAtv(MultVec3(Mult(camera_rotation_vertical, camera_rotation_side), pos), lookat, rotnormal);
	glUniformMatrix4fv(glGetUniformLocation(program1, "cameraMatrix1"), 1, GL_TRUE, camera.m);
	
	glUniformMatrix4fv(glGetUniformLocation(program1, "transformMatrix1"), 1, GL_TRUE, walls_transform.m);	
	DrawModel(walls, program1, "in_vertex1",  "in_normal1", "in_texture1");

	glUniformMatrix4fv(glGetUniformLocation(program1, "transformMatrix1"), 1, GL_TRUE, roof_transform.m);	
	DrawModel(roof, program1, "in_vertex1",  "in_normal1", "in_texture1");
	
	glUniformMatrix4fv(glGetUniformLocation(program1, "transformMatrix1"), 1, GL_TRUE, balcony_transform.m);	
	DrawModel(balcony, program1, "in_vertex1",  "in_normal1", "in_texture1");
	
	blade_rot = Rx(PI/2);
	blade_turning = Rx(angle);

	
	blade_turning = Mult(blade_turning, blade_rot);
	blade_transform = Mult(blade_pos, blade_turning); 
	glUniformMatrix4fv(glGetUniformLocation(program1, "transformMatrix1"), 1, GL_TRUE, blade_transform.m);	
	DrawModel(blade, program1, "in_vertex1",  "in_normal1", "in_texture1");


	blade_turning = Mult(blade_turning, blade_rot);
	blade_transform = Mult(blade_pos, blade_turning); 
	glUniformMatrix4fv(glGetUniformLocation(program1, "transformMatrix1"), 1, GL_TRUE, blade_transform.m);	
	DrawModel(blade, program1, "in_vertex1",  "in_normal1", "in_texture1");

	blade_turning = Mult(blade_turning, blade_rot);
	blade_transform = Mult(blade_pos, blade_turning); 
	glUniformMatrix4fv(glGetUniformLocation(program1, "transformMatrix1"), 1, GL_TRUE, blade_transform.m);	
	DrawModel(blade, program1, "in_vertex1",  "in_normal1", "in_texture1");

	blade_turning = Mult(blade_turning, blade_rot);
	blade_transform = Mult(blade_pos, blade_turning); 
	glUniformMatrix4fv(glGetUniformLocation(program1, "transformMatrix1"), 1, GL_TRUE, blade_transform.m);	
	DrawModel(blade, program1, "in_vertex1",  "in_normal1", "in_texture1");


	printError("display");
	angle= angle +0.03;
	glutSwapBuffers();
}

int main(int argc, char *argv[])
{
	glutInit(&argc, argv);
	glutInitContextVersion(3, 2);
	glutCreateWindow ("GL3 white triangle example");
	glutDisplayFunc(display); 
	init ();
	glutTimerFunc(20, &OnTimer, 0);
	glutMainLoop();
	return 0;
}
