#version 150

in  vec3 inPosition;
in  vec3 inNormal;
in vec2 inTexCoord;
out vec2 texCoord;
out vec3 outNormal;
out vec4 vertex_pos;
out vec3 outPosition;

// NY
uniform mat4 projMatrix;
uniform mat4 mdlMatrix;

void main(void)
{
	mat3 normalMatrix1 = mat3(mdlMatrix);
	texCoord = inTexCoord;
	outNormal = inNormal;
	vec4 vertex_pos_transform =  projMatrix * mdlMatrix * vec4(inPosition, 1.0);
	vertex_pos = vertex_pos_transform;
	outPosition = inPosition;
	gl_Position = vertex_pos_transform;
}
