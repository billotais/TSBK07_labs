# Question 1
The heightmap data holds the height of all the vertices of the terrain, value between 0 and 1
4x4 vertices => 9 squares => 18 triangles
# Question 2
yes, up-down, rotate and forward when rotated
# Question 3
Find 3 adjacent vertices, normal vector of surface using cross product
# Question 4
word coordinate, divide by scale, diffx + diffz < 1 for which triangle
Interpolation vectors from side of triangle
# Question 5
Depending on the height of the input vertices, water down (<0.5)
Grass < 10
Rock on top
