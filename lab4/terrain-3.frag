#version 150

out vec4 outColor;
in vec3 outNormal;
in vec2 texCoord;
in vec4 vertex_pos;
uniform sampler2D tex;

uniform mat4 projMatrix;
uniform mat4 mdlMatrix;

void main(void)
{
	vec3 temp_color = vec3(0, 0, 0);
	vec3 light_level = vec3(1.0, 0.0, 1.0);
	vec3 reflectivity = vec3(0.7, 0.7, 0.7);
	float  specularExponent = 1;

	vec3 light_transformed = vec3(projMatrix*mdlMatrix*vec4(0, -20, 0, 1.0));
	vec3 vertex_to_light;
		
	vertex_to_light = normalize(light_transformed - vec3(vertex_pos));
		

		
	float costheta = dot(normalize(outNormal),vertex_to_light);
	vec3 diffuse = vec3(max(0, reflectivity.x*light_level.x*costheta), max(0, reflectivity.y*light_level.y*costheta),max			(0,reflectivity.z*light_level.z*costheta));

	vec3 r = normalize(2*normalize(outNormal*(dot(vertex_to_light,normalize(outNormal)))) - vertex_to_light);
	vec3 vertex_to_camera = normalize(vec3(projMatrix*mdlMatrix*mdlMatrix[0]) - vec3(vertex_pos));
	vec3 specular = vec3(reflectivity.x*light_level.x*pow(max(0,dot(r, vertex_to_camera)), specularExponent),
							reflectivity.y*light_level.y*pow(max(0,dot(r, vertex_to_camera)), specularExponent),
							reflectivity.z*light_level.z*pow(max(0,dot(r, vertex_to_camera)), specularExponent));

		/*vec3 specular = vec3(reflectivity.x*light_level.x*pow(abs(dot(r, vertex_to_light)), specularExponent),
							reflectivity.y*light_level.y*pow(abs(dot(r, vertex_to_light)), specularExponent),
							reflectivity.z*light_level.z*pow(abs(dot(r, vertex_to_light)), specularExponent));*/


	temp_color += diffuse + specular;

	






	outColor = vec4(temp_color,1.0);
}
