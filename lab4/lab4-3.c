// Lab 4, terrain generation

#ifdef __APPLE__
	#include <OpenGL/gl3.h>
	// Linking hint for Lightweight IDE
	// uses framework Cocoa
#endif
#include "MicroGlut.h"
#include "GL_utilities.h"
#include "VectorUtils3.h"
#include "loadobj.h"
#include "LoadTGA.h"
#include <math.h>

#define PI 3.141592
mat4 projectionMatrix;


double camera_angle = 0.0;

vec3 camera_pos;
vec3 camera_rot;
vec3 camera_lookat;
mat4 camera;

Model* GenerateTerrain(TextureData *tex)
{
	int vertexCount = tex->width * tex->height;
	int triangleCount = (tex->width-1) * (tex->height-1) * 2;
	int x, z;

	GLfloat *vertexArray = malloc(sizeof(GLfloat) * 3 * vertexCount);
	GLfloat *normalArray = malloc(sizeof(GLfloat) * 3 * vertexCount);
	GLfloat *texCoordArray = malloc(sizeof(GLfloat) * 2 * vertexCount);
	GLuint *indexArray = malloc(sizeof(GLuint) * triangleCount*3);

	printf("bpp %d\n", tex->bpp);
	for (x = 0; x < tex->width; x++)
		for (z = 0; z < tex->height; z++)
		{
// Vertex array. You need to scale this properly
			vertexArray[(x + z * tex->width)*3 + 0] = x / 10.0;
			vertexArray[(x + z * tex->width)*3 + 1] = tex->imageData[(x + z * tex->width) * (tex->bpp/8)] / 100.0;
			vertexArray[(x + z * tex->width)*3 + 2] = z / 10.0;
// Normal vectors. You need to calculate these.


			if (0 < x && x < tex->width - 1 && 0 < z && z < tex->width - 1)
			{
				vec3 top_point;
				top_point.x = vertexArray[(x + (z-1) * tex->width)*3 + 0];
				top_point.y = vertexArray[(x + (z-1) * tex->width)*3 + 1];
				top_point.z = vertexArray[(x + (z-1) * tex->width)*3 + 2];

				vec3 bot_left_point;
				bot_left_point.x = vertexArray[((x-1) + (z+1) * tex->width)*3 + 0];
				bot_left_point.y = vertexArray[((x-1) + (z+1) * tex->width)*3 + 1];
				bot_left_point.z = vertexArray[((x-1) + (z+1) * tex->width)*3 + 2];

				vec3 bot_right_point;
				bot_right_point.x = vertexArray[((x+1) + (z+1) * tex->width)*3 + 0];
				bot_right_point.y = vertexArray[((x+1) + (z+1) * tex->width)*3 + 1];
				bot_right_point.z = vertexArray[((x+1) + (z+1) * tex->width)*3 + 2];

				vec3 normal_vec = Normalize(CrossProduct(VectorSub(bot_left_point, top_point),VectorSub(bot_right_point, top_point)));
				normalArray[(x + z * tex->width)*3 + 0] = normal_vec.x;
				normalArray[(x + z * tex->width)*3 + 1] = normal_vec.y;
				normalArray[(x + z * tex->width)*3 + 2] = normal_vec.z;
			}
			else
			{
				normalArray[(x + z * tex->width)*3 + 0] = 0.0;
				normalArray[(x + z * tex->width)*3 + 1] = 1.0;
				normalArray[(x + z * tex->width)*3 + 2] = 0.0;
			}
			
// Texture coordinates. You may want to scale them.
			texCoordArray[(x + z * tex->width)*2 + 0] =  (float)x / tex->width;
			texCoordArray[(x + z * tex->width)*2 + 1] =  (float)z / tex->height;
		}
	for (x = 0; x < tex->width-1; x++)
		for (z = 0; z < tex->height-1; z++)
		{
		// Triangle 1
			indexArray[(x + z * (tex->width-1))*6 + 0] = x + z * tex->width;
			indexArray[(x + z * (tex->width-1))*6 + 1] = x + (z+1) * tex->width;
			indexArray[(x + z * (tex->width-1))*6 + 2] = x+1 + z * tex->width;
		// Triangle 2
			indexArray[(x + z * (tex->width-1))*6 + 3] = x+1 + z * tex->width;
			indexArray[(x + z * (tex->width-1))*6 + 4] = x + (z+1) * tex->width;
			indexArray[(x + z * (tex->width-1))*6 + 5] = x+1 + (z+1) * tex->width;
		}

	// End of terrain generation

	// Create Model and upload to GPU:

	Model* model = LoadDataToModel(
			vertexArray,
			normalArray,
			texCoordArray,
			NULL,
			indexArray,
			vertexCount,
			triangleCount*3);


	return model;

}


// vertex array object
Model *m, *m2, *tm;
// Reference to shader program
GLuint program;
GLuint tex1, tex2;
TextureData ttex; // terrain

void init(void)
{
	// GL inits
	glClearColor(0.2,0.2,0.5,0);
	glEnable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);
	printError("GL inits");


	camera_pos.x = 20;camera_pos.y = 4.0;camera_pos.z=20;
	camera_lookat.x = 2.0; camera_lookat.y = 4.0; camera_lookat.z = 2.0;
	camera_rot.x = 0.0; camera_rot.y = 1.0; camera_rot.z = 0.0;

	projectionMatrix = frustum(-0.1, 0.1, -0.1, 0.1, 0.2, 50.0);

	// Load and compile shader
	program = loadShaders("terrain-3.vert", "terrain-3.frag");
	glUseProgram(program);
	printError("init shader");

	glUniformMatrix4fv(glGetUniformLocation(program, "projMatrix"), 1, GL_TRUE, projectionMatrix.m);
	glUniform1i(glGetUniformLocation(program, "tex"), 0); // Texture unit 0
	LoadTGATextureSimple("maskros512.tga", &tex1);

// Load terrain data
	LoadTGATextureData("fft-terrain.tga", &ttex);
	tm = GenerateTerrain(&ttex);
	printError("init terrain");
}

void display(void)
{
	// clear the screen
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	mat4 total, modelView, camMatrix;

	printError("pre display");

	glUseProgram(program);

	// Build matrix

	if (glutKeyIsDown('w'))
	{
		vec3 move = ScalarMult(VectorSub(camera_lookat,camera_pos), 0.1);
		camera_pos = VectorAdd(camera_pos, move);
		camera_lookat = VectorAdd(camera_lookat, move);
	}
	if (glutKeyIsDown('s'))
	{
		vec3 move = ScalarMult(VectorSub(camera_lookat,camera_pos), 0.1);
		camera_pos = VectorSub(camera_pos, move);
		camera_lookat = VectorSub(camera_lookat, move);
	}
	if (glutKeyIsDown('a'))
	{
		vec3 move = MultVec3(Ry(PI/2),ScalarMult(VectorSub(camera_lookat,camera_pos), 0.1));
		camera_pos = VectorAdd(camera_pos, move);
		camera_lookat = VectorAdd(camera_lookat, move);
	}
	if (glutKeyIsDown('d'))
	{
		vec3 move = MultVec3(Ry(PI/2),ScalarMult(VectorSub(camera_lookat,camera_pos), 0.1));
		camera_pos = VectorSub(camera_pos, move);
		camera_lookat = VectorSub(camera_lookat, move);
	}
	if (glutKeyIsDown('e'))
	{
		
		vec3 cam_to_lookpoint = VectorSub(camera_lookat,camera_pos);
		vec3 new_cam_to_lookpoint = MultVec3(Ry(-0.1), cam_to_lookpoint);
		camera_lookat = VectorAdd(camera_pos, new_cam_to_lookpoint); 
	}
	if (glutKeyIsDown('q'))
	{
		
		vec3 cam_to_lookpoint = VectorSub(camera_lookat,camera_pos);
		vec3 new_cam_to_lookpoint = MultVec3(Ry(0.1), cam_to_lookpoint);
		camera_lookat = VectorAdd(camera_pos, new_cam_to_lookpoint); 
	}
	float x_coord_terrain = camera_pos.x*10;
	float z_coord_terrain = camera_pos.z*10;
	float new_height;
	if (x_coord_terrain + z_coord_terrain < 1) // Top-left triangle
	{
		float height_t_l = ttex.imageData[(int)floor((camera_pos.x*10 + camera_pos.z*10 * ttex.width) * (ttex.bpp/8))] / 100.0;
		float height_t_r = ttex.imageData[(int)floor(((camera_pos.x*10+1) + camera_pos.z*10 * ttex.width) * (ttex.bpp/8))] / 100.0;
		float height_b_l = ttex.imageData[(int)floor((camera_pos.x*10 + (camera_pos.z*10+1) * ttex.width) * (ttex.bpp/8))] / 100.0;
		
		float vert_diff = height_b_l - height_t_l;
		float hor_diff = height_t_r - height_t_l;
		
		
		new_height = height_t_l + hor_diff*(x_coord_terrain - floor(x_coord_terrain)) + vert_diff*(z_coord_terrain - floor(z_coord_terrain));
	}
	else // bot-right triangle
	{
		float height_b_r = ttex.imageData[(int)floor(((camera_pos.x*10+1) + (camera_pos.z*10+1) * ttex.width) * (ttex.bpp/8))] / 100.0;
		float height_t_r = ttex.imageData[(int)floor(((camera_pos.x*10+1) + camera_pos.z*10 * ttex.width) * (ttex.bpp/8))] / 100.0;
		float height_b_l = ttex.imageData[(int)floor((camera_pos.x*10 + (camera_pos.z*10+1) * ttex.width) * (ttex.bpp/8))] / 100.0;
		
		float vert_diff = height_t_r - height_b_r;
		float hor_diff = height_b_l - height_b_r;
		
		new_height = height_b_r + hor_diff*(x_coord_terrain - floor(x_coord_terrain)) + vert_diff*(z_coord_terrain - floor(z_coord_terrain));
	}
	
	
	camera_pos.y = new_height + 0.5;
	camera_lookat.y = new_height + 0.5;

	camMatrix = lookAtv(camera_pos, camera_lookat, camera_rot);
	modelView = IdentityMatrix();
	total = Mult(camMatrix, modelView);
	glUniformMatrix4fv(glGetUniformLocation(program, "mdlMatrix"), 1, GL_TRUE, total.m);

	glBindTexture(GL_TEXTURE_2D, tex1);		// Bind Our Texture tex1
	DrawModel(tm, program, "inPosition", "inNormal", "inTexCoord");

	printError("display 2");

	glutSwapBuffers();
}

void timer(int i)
{
	glutTimerFunc(20, &timer, i);
	glutPostRedisplay();
}

void mouse(int x, int y)
{
	printf("%d %d\n", x, y);
}

int main(int argc, char **argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_DEPTH);
	glutInitContextVersion(3, 2);
	glutInitWindowSize (600, 600);
	glutCreateWindow ("TSBK07 Lab 4");
	glutDisplayFunc(display);
	init ();
	glutTimerFunc(20, &timer, 0);

	glutPassiveMotionFunc(mouse);

	glutMainLoop();
	exit(0);
}
