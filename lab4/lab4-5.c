// Lab 4, terrain generation

#ifdef __APPLE__
	#include <OpenGL/gl3.h>
	// Linking hint for Lightweight IDE
	// uses framework Cocoa
#endif
#include "MicroGlut.h"
#include "GL_utilities.h"
#include "VectorUtils3.h"
#include "loadobj.h"
#include "LoadTGA.h"
#include <math.h>

#define PI 3.141592
mat4 projectionMatrix;


double camera_angle = 0.0;
float sphere_angle = 0.0;

vec3 camera_pos;
vec3 camera_rot;
vec3 camera_lookat;
mat4 camera;

mat4 sphere_transform;
mat4 sphere_position;
mat4 sphere_center;

Model* sphere;


Model* GenerateTerrain(TextureData *tex)
{
	int vertexCount = tex->width * tex->height;
	int triangleCount = (tex->width-1) * (tex->height-1) * 2;
	int x, z;

	GLfloat *vertexArray = malloc(sizeof(GLfloat) * 3 * vertexCount);
	GLfloat *normalArray = malloc(sizeof(GLfloat) * 3 * vertexCount);
	GLfloat *texCoordArray = malloc(sizeof(GLfloat) * 2 * vertexCount);
	GLuint *indexArray = malloc(sizeof(GLuint) * triangleCount*3);

	printf("bpp %d\n", tex->bpp);
	for (x = 0; x < tex->width; x++)
		for (z = 0; z < tex->height; z++)
		{
// Vertex array. You need to scale this properly
			vertexArray[(x + z * tex->width)*3 + 0] = x / 1.0;
			vertexArray[(x + z * tex->width)*3 + 1] = tex->imageData[(x + z * tex->width) * (tex->bpp/8)] / 20.0;
			vertexArray[(x + z * tex->width)*3 + 2] = z / 1.0;
// Normal vectors. You need to calculate these.


			if (0 < x && x < tex->width - 1 && 0 < z && z < tex->width - 1)
			{
				vec3 top_point;
				top_point.x = vertexArray[(x + (z-1) * tex->width)*3 + 0];
				top_point.y = vertexArray[(x + (z-1) * tex->width)*3 + 1];
				top_point.z = vertexArray[(x + (z-1) * tex->width)*3 + 2];

				vec3 bot_left_point;
				bot_left_point.x = vertexArray[((x-1) + (z+1) * tex->width)*3 + 0];
				bot_left_point.y = vertexArray[((x-1) + (z+1) * tex->width)*3 + 1];
				bot_left_point.z = vertexArray[((x-1) + (z+1) * tex->width)*3 + 2];

				vec3 bot_right_point;
				bot_right_point.x = vertexArray[((x+1) + (z+1) * tex->width)*3 + 0];
				bot_right_point.y = vertexArray[((x+1) + (z+1) * tex->width)*3 + 1];
				bot_right_point.z = vertexArray[((x+1) + (z+1) * tex->width)*3 + 2];

				vec3 normal_vec = Normalize(CrossProduct(VectorSub(bot_left_point, top_point),VectorSub(bot_right_point, top_point)));
				normalArray[(x + z * tex->width)*3 + 0] = normal_vec.x;
				normalArray[(x + z * tex->width)*3 + 1] = normal_vec.y;
				normalArray[(x + z * tex->width)*3 + 2] = normal_vec.z;
			}
			else
			{
				normalArray[(x + z * tex->width)*3 + 0] = 0.0;
				normalArray[(x + z * tex->width)*3 + 1] = 1.0;
				normalArray[(x + z * tex->width)*3 + 2] = 0.0;
			}
			
// Texture coordinates. You may want to scale them.
			texCoordArray[(x + z * tex->width)*2 + 0] =  (float)x / tex->width;
			texCoordArray[(x + z * tex->width)*2 + 1] =  (float)z / tex->height;
		}
	for (x = 0; x < tex->width-1; x++)
		for (z = 0; z < tex->height-1; z++)
		{
		// Triangle 1
			indexArray[(x + z * (tex->width-1))*6 + 0] = x + z * tex->width;
			indexArray[(x + z * (tex->width-1))*6 + 1] = x + (z+1) * tex->width;
			indexArray[(x + z * (tex->width-1))*6 + 2] = x+1 + z * tex->width;
		// Triangle 2
			indexArray[(x + z * (tex->width-1))*6 + 3] = x+1 + z * tex->width;
			indexArray[(x + z * (tex->width-1))*6 + 4] = x + (z+1) * tex->width;
			indexArray[(x + z * (tex->width-1))*6 + 5] = x+1 + (z+1) * tex->width;
		}

	// End of terrain generation

	// Create Model and upload to GPU:

	Model* model = LoadDataToModel(
			vertexArray,
			normalArray,
			texCoordArray,
			NULL,
			indexArray,
			vertexCount,
			triangleCount*3);


	return model;

}


// vertex array object
Model *m, *m2, *tm;
// Reference to shader program
GLuint program;
GLuint tex1, tex2;
TextureData ttex; // terrain

void init(void)
{
	// GL inits
	glClearColor(0.2,0.2,0.5,0);
	glEnable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);
	printError("GL inits");


	camera_pos.x = 0;camera_pos.y = 4.0;camera_pos.z=8;
	camera_lookat.x = 2.0; camera_lookat.y = 4.0; camera_lookat.z = 2.0;
	camera_rot.x = 0.0; camera_rot.y = 1.0; camera_rot.z = 0.0;

	projectionMatrix = frustum(-0.1, 0.1, -0.1, 0.1, 0.2, 500.0);


	sphere = LoadModelPlus("groundsphere.obj");
	sphere_center = T(50, 0, 50);
	sphere_position = T(25, 0, 25);
	
	// Load and compile shader
	program = loadShaders("terrain-5.vert", "terrain-5.frag");
	glUseProgram(program);
	printError("init shader");
	
	glUniformMatrix4fv(glGetUniformLocation(program, "projMatrix"), 1, GL_TRUE, projectionMatrix.m);
	glUniform1i(glGetUniformLocation(program, "tex"), 0); // Texture unit 0
	LoadTGATextureSimple("grass.tga", &tex1);
	LoadTGATextureSimple("conc.tga", &tex2);
	
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, tex1);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, tex2);

// Load terrain data
	LoadTGATextureData("fft-terrain.tga", &ttex);
	tm = GenerateTerrain(&ttex);
	printError("init terrain");
}

void display(void)
{
	// clear the screen
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	mat4 total, modelView, camMatrix;

	printError("pre display");

	glUseProgram(program);

	// Build matrix

	if (glutKeyIsDown('w'))
	{
		vec3 move = ScalarMult(VectorSub(camera_lookat,camera_pos), 0.05);
		camera_pos = VectorAdd(camera_pos, move);
		camera_lookat = VectorAdd(camera_lookat, move);
	}
	if (glutKeyIsDown('s'))
	{
		vec3 move = ScalarMult(VectorSub(camera_lookat,camera_pos), 0.05);
		camera_pos = VectorSub(camera_pos, move);
		camera_lookat = VectorSub(camera_lookat, move);
	}
	if (glutKeyIsDown('a'))
	{
		vec3 move = MultVec3(Ry(PI/2),ScalarMult(VectorSub(camera_lookat,camera_pos), 0.05));
		camera_pos = VectorAdd(camera_pos, move);
		camera_lookat = VectorAdd(camera_lookat, move);
	}
	if (glutKeyIsDown('d'))
	{
		vec3 move = MultVec3(Ry(PI/2),ScalarMult(VectorSub(camera_lookat,camera_pos), 0.05));
		camera_pos = VectorSub(camera_pos, move);
		camera_lookat = VectorSub(camera_lookat, move);
	}
	if (glutKeyIsDown('e'))
	{
		
		vec3 cam_to_lookpoint = VectorSub(camera_lookat,camera_pos);
		vec3 new_cam_to_lookpoint = MultVec3(Ry(-0.2), cam_to_lookpoint);
		camera_lookat = VectorAdd(camera_pos, new_cam_to_lookpoint); 
	}
	if (glutKeyIsDown('q'))
	{
		
		vec3 cam_to_lookpoint = VectorSub(camera_lookat,camera_pos);
		vec3 new_cam_to_lookpoint = MultVec3(Ry(0.2), cam_to_lookpoint);
		camera_lookat = VectorAdd(camera_pos, new_cam_to_lookpoint); 
	}
	if (glutKeyIsDown('r'))
	{
		
		camera_lookat = VectorAdd(camera_lookat, SetVector(0.0, 0.05, 0.0));
		camera_pos = VectorAdd(camera_pos, SetVector(0.0, 0.05, 0.0));  
	}
	if (glutKeyIsDown('f'))
	{
		
		camera_lookat = VectorAdd(camera_lookat, SetVector(0.0, -0.05, 0.0));
		camera_pos = VectorAdd(camera_pos, SetVector(0.0, -0.05, 0.0));  
	}
	
	sphere_transform = Mult(sphere_center, Mult(Ry(sphere_angle), sphere_position));
	
	vec3 coord_terrain = MultVec3(sphere_transform,SetVector(0, 0, 0));
	int x_coord_terrain = floor(coord_terrain.x);
	int z_coord_terrain = floor(coord_terrain.z);
	//int new_height;
	vec3 interpolated_vec;
	if (x_coord_terrain - floor(x_coord_terrain) + z_coord_terrain - floor(z_coord_terrain) < 1) // Top-left triangle
	{
		float height_t_l = ttex.imageData[(int)floor((x_coord_terrain + z_coord_terrain * ttex.width) * (ttex.bpp/8))] / 20.0;
		float height_t_r = ttex.imageData[(int)floor(((x_coord_terrain+1) + z_coord_terrain * ttex.width) * (ttex.bpp/8))] / 20.0;
		float height_b_l = ttex.imageData[(int)floor((x_coord_terrain + (z_coord_terrain+1) * ttex.width) * (ttex.bpp/8))] / 20.0;
		
		vec3 vec_t_l = SetVector(x_coord_terrain, height_t_l,  z_coord_terrain);
		vec3 vec_t_r = SetVector(x_coord_terrain+1, height_t_r,  z_coord_terrain);
		vec3 vec_b_l = SetVector(x_coord_terrain, height_b_l,  z_coord_terrain+1);
		
		float hor_diff = coord_terrain.x - x_coord_terrain;
		float ver_diff = coord_terrain.z - z_coord_terrain;
		
		
		//new_height = height_t_l + hor_diff*(coord_terrain.x - floor(x_coord_terrain)) + vert_diff*(coord_terrain.z - floor(z_coord_terrain));
		
		
		
		interpolated_vec = VectorAdd(VectorAdd(vec_t_l,  ScalarMult(VectorSub(vec_t_r, vec_t_l), hor_diff)),  ScalarMult(VectorSub(vec_b_l, vec_t_l), ver_diff));
	}
	else // bot-right triangle
	{
		float height_b_r = ttex.imageData[(int)floor(((x_coord_terrain+1) + (z_coord_terrain+1) * ttex.width) * (ttex.bpp/8))] / 20.0;
		float height_t_r = ttex.imageData[(int)floor(((x_coord_terrain+1) + z_coord_terrain * ttex.width) * (ttex.bpp/8))] / 20.0;
		float height_b_l = ttex.imageData[(int)floor((x_coord_terrain + (z_coord_terrain+1) * ttex.width) * (ttex.bpp/8))] / 20.0;
		/*
		float vert_diff = height_t_r - height_b_r;
		float hor_diff = height_b_l - height_b_r;
		*/
		//new_height = height_b_r + hor_diff*(x_coord_terrain+1 - coord_terrain.x) + vert_diff*(z_coord_terrain+1 - coord_terrain.z);
		
		vec3 vec_b_r = SetVector(x_coord_terrain+1, height_b_r,  z_coord_terrain+1);
		vec3 vec_t_r = SetVector(x_coord_terrain+1, height_t_r,  z_coord_terrain);
		vec3 vec_b_l = SetVector(x_coord_terrain, height_b_l,  z_coord_terrain+1);
		
		float hor_diff = x_coord_terrain - coord_terrain.x;
		float ver_diff = z_coord_terrain - coord_terrain.z;
		
		
		//new_height = height_t_l + hor_diff*(coord_terrain.x - floor(x_coord_terrain)) + vert_diff*(coord_terrain.z - floor(z_coord_terrain));
		
		
		
		interpolated_vec = VectorAdd(VectorAdd(vec_b_r,  ScalarMult( VectorSub(vec_b_r, vec_b_l), hor_diff)),  ScalarMult(VectorSub(vec_b_r, vec_t_r), ver_diff));
	}
	
	float new_height = interpolated_vec.y;
	//camera_pos.y = new_height + 0.5;
	//camera_lookat.y = new_height + 0.5;
	
	
	sphere_transform = Mult(Mult(T(0.0, new_height, 0.0), sphere_center), Mult(Ry(sphere_angle), sphere_position));
	camMatrix = lookAtv(camera_pos, camera_lookat, camera_rot);
	modelView = IdentityMatrix();
	total = Mult(camMatrix, modelView);
	glUniformMatrix4fv(glGetUniformLocation(program, "mdlMatrix"), 1, GL_TRUE, total.m);
	glUniform1i(glGetUniformLocation(program, "texUnit_0"), 0);
	glUniform1i(glGetUniformLocation(program, "texUnit_1"), 1);
	
	DrawModel(tm, program, "inPosition", "inNormal", "inTexCoord");
	
	total = Mult(Mult(camMatrix, modelView), sphere_transform);
	glUniformMatrix4fv(glGetUniformLocation(program, "mdlMatrix"), 1, GL_TRUE, total.m);
	DrawModel(sphere, program, "inPosition", "inNormal", "inTexCoord");
	
	

	sphere_angle+= 0.01;
	printError("display 2");

	glutSwapBuffers();
}

void timer(int i)
{
	glutTimerFunc(20, &timer, i);
	glutPostRedisplay();
}

void mouse(int x, int y)
{
	printf("%d %d\n", x, y);
}

int main(int argc, char **argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_DEPTH);
	glutInitContextVersion(3, 2);
	glutInitWindowSize (600, 600);
	glutCreateWindow ("TSBK07 Lab 4");
	glutDisplayFunc(display);
	init ();
	glutTimerFunc(20, &timer, 0);

	glutPassiveMotionFunc(mouse);

	glutMainLoop();
	exit(0);
}
